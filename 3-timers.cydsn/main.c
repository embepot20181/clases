/* ========================================
 *
 * Copyright YOUR COMPANY, THE YEAR
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF your company.
 *
 * ========================================
*/
#include "project.h"
#include "Timer_1.h"
#include "Timer_2.h"

int main(void)
{
    CyGlobalIntEnable; /* Enable global interrupts. */

    /* Place your initialization/startup code here (e.g. MyInst_Start()) */
    Timer_1_Start();
    Timer_2_Start();

    for(;;)
    {
        /* Place your application code here. */
        while (!(Timer_1_ReadStatusRegister() & Timer_1_STATUS_TC));
        LED1_Write(!LED1_Read());
    }
}

/* [] END OF FILE */

/* ========================================
 *
 * Copyright YOUR COMPANY, THE YEAR
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF your company.
 *
 * ========================================
Amplitud con el potenciometro
Cambio de frecuencia con pulsadores

Mecánica:
- Hacer el periodo del Timer igual al de 256 veces la frecuencia deseada (para las 256 muestras)
- En cada flanco de subida del timer el DAC se actualiza con el registro de control
- En cada flanco de bajada del timer se actualiza el registro de control, y se incrementa el indice.
- Cada medio segundo se hace una conversion ADC. Una vez completada, se recalcula toda la tabla
-
UPD_DAC lee el siguiente valor de la tabla y lo escribe en el DAC

*/
#include "project.h"
#include <math.h>
#include <stdbool.h>

#define PI 3.14159265

char tabla[256],seno[256];
volatile char amplitud;
volatile bool cambio = true;

//Asigna el siguiente valor de
CY_ISR(int_INTENTODMA) {
    static char i=0;
    DACVAL_Write(tabla[i++]);
    //LEDS_Write(LEDS_ReadDataReg() | 1);
}

//Lee el valor de conversion del ADC
CY_ISR(int_AMPLITUD) {
    amplitud=ADC_GetResult8();
    cambio = true;
    //LEDS_Write(LEDS_ReadDataReg() | 2);
}

//Deben actualizar el periodo del timer
CY_ISR(int_SUBIR) {
    unsigned long periodo = TIMER_ReadPeriod();
    TIMER_WritePeriod((periodo == 0)?periodo:periodo-1);
    //LEDS_Write(LEDS_ReadDataReg() | 4);
}
CY_ISR(int_BAJAR) {
    unsigned long periodo = TIMER_ReadPeriod();
    TIMER_WritePeriod((periodo == 4294967295)?periodo:periodo+1);
    //LEDS_Write(LEDS_ReadDataReg() | 8);
}

int main(void)
{
    CyGlobalIntEnable; /* Enable global interrupts. */

    int i;
    double valor;
    UPD_AMPL_StartEx(int_AMPLITUD);
    UPD_DAC_StartEx(int_INTENTODMA);
    UP_REQ_StartEx(int_SUBIR);
    DOWN_REQ_StartEx(int_BAJAR);

    ADC_Start();
    TIMER_Start();
    DAC_Start();

    for (i=0;i<256;i++)
        seno[i]=(char)((sin(2.0*PI*i/255.0)+1)*255.0/2.0);

    /* Place your initialization/startup code here (e.g. MyInst_Start()) */
    for(;;)
    {
        if (cambio) {
            cambio = false;
            char forma = FORMA_Read();
            for (i=0;i<256;i++) {
                //FORMA DE ONDA
                switch (forma) {
                    case 0: //SIERRA
                        tabla[i]=i;
                        break;
                    case 1: //CUADRADA
                        tabla[i]=(i<128)?255:0;
                        break;
                    case 2: //TRIANGULAR
                        tabla[i]=(i<128)?2*i:2*(255-i);
                        break;
                    case 3: //SENO
                        tabla[i] = seno[i];
                    default:
                        break;
                }
                //AMPLITUD
                tabla[i] = (char)(tabla[i]*amplitud/255.0);
            }
        }
    }
}

/* [] END OF FILE */

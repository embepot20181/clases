/* ========================================
 *
 * Copyright YOUR COMPANY, THE YEAR
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF your company.
 *
 * ========================================
*/
#include "project.h"
uint16 curpos;

int main(void)
{
    CyGlobalIntEnable; /* Enable global interrupts. */
    CapSense_Start();
    CapSense_InitializeAllBaselines(); //Inicia componentes del capsense

    /* Place your initialization/startup code here (e.g. MyInst_Start()) */

    for(;;)
    {
        // Place your application code here.
        
        CapSense_UpdateEnabledBaselines();  // Como los condensadores se van descargando,
                                            // con este comando se refrescan...
        CapSense_ScanEnabledWidgets();  // Inicio del escaneo de botones
        while (CapSense_IsBusy());
        /*
        if (CapSense_CheckIsWidgetActive(CapSense_BUTTON0__BTN)) //Se verifica que en efecto el boton esta activo
        {
            LED_Write(1);
            //CyDelay(200);
        }
        if (CapSense_CheckIsWidgetActive(CapSense_BUTTON1__BTN)) //Se verifica que en efecto el boton esta activo
        {
            LED_Write(0);
        }
        // */
        CapSense_GetCentroidPos();
    }
}

/* [] END OF FILE */

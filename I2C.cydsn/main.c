/* ========================================
 *  I2C con la brujula CMPS10
 * ========================================
 */
#include "project.h"
#include <stdio.h>
#define DIR_ACELEROMETRO    0x60

uint8 result,i,tabla[4];
uint16 angulo;

int main(void)
{
    CyGlobalIntEnable; /* Enable global interrupts. */
    LCD_Start();
    I2C_Start();
    /* Place your initialization/startup code here (e.g. MyInst_Start()) */

    for(;;)
    {
        /* Place your application code here. */
        do {
            result=I2C_MasterSendStart(DIR_ACELEROMETRO,I2C_WRITE_XFER_MODE);
        } while(result != I2C_MSTR_NO_ERROR);
        
        I2C_MasterWriteByte(1);
        I2C_MasterSendRestart(DIR_ACELEROMETRO,I2C_READ_XFER_MODE);
        result=I2C_MasterReadByte(I2C_NAK_DATA);
        I2C_MasterSendStop();
        
        LCD_ClearDisplay();
        LCD_Position(0,0);
        LCD_PrintString("SD: ");
        LCD_PrintNumber(result);
        
        //lectura multiples posiciones
        
        do {
            result=I2C_MasterSendStart(DIR_ACELEROMETRO,I2C_WRITE_XFER_MODE);
        } while(result != I2C_MSTR_NO_ERROR);
        
        I2C_MasterWriteByte(0x02);
        I2C_MasterSendRestart(DIR_ACELEROMETRO,I2C_READ_XFER_MODE);
        for(i=0;i<3;i++){
            tabla[i]=I2C_MasterReadByte(I2C_ACK_DATA);
        }
        tabla[3]=I2C_MasterReadByte(I2C_NAK_DATA);
        I2C_MasterSendStop();
        
        angulo =(uint16)tabla[0]<<8;
        angulo += (uint16)tabla[1];
        
        char buff[20],len;
        len=snprintf(buff,20,"%i",angulo);
        LCD_Position(1,0);
        LCD_PrintString("HD: ");
        for (i=0;i<len;i++){
            LCD_PutChar(buff[i]);
        }
        CyDelay(1000);
    }
}

/* [] END OF FILE */

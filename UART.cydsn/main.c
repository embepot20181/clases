/* ========================================
 *
 * Copyright YOUR COMPANY, THE YEAR
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF your company.
 *
 * ========================================
*/
#include "project.h"

int main(void)
{
    CyGlobalIntEnable; /* Enable global interrupts. */

    /* Place your initialization/startup code here (e.g. MyInst_Start()) */
    LCD_Start();
    UART_Start();
    
    char fila=0;
    LCD_Position(fila,0);
    //LCD_PrintString("ola k ase");

    for(;;)
    {
        /* Place your application code here. */
        //UART_PutString("MENSAje de prueba enviado por PSoC 5LP a 38400 baudios\n\r");
        //CyDelay(5000);
        char dato;
        while(!(dato=UART_GetChar()));
        if (dato=='\n') {
        } else if (dato=='\r') {
            fila=fila?0:1;
            LCD_Position(fila,0);
        } else {
            LCD_PutChar(dato);
        }
        UART_PutChar(dato);
    }
}

/* [] END OF FILE */

/* ========================================
 *
 * Copyright YOUR COMPANY, THE YEAR
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF your company.
 *
 * ========================================
*/
#include "project.h"

uint16 tiempo;

int main(void)
{
    //CyGlobalIntEnable; /* Enable global interrupts. */

    /* Place your initialization/startup code here (e.g. MyInst_Start()) */
    tiempo = 1000;
    for(;;)
    {
        /* Place your application code here. */
        LED_Write(~LED_Read());
        CyDelay(tiempo);
        LED_Write(~LED_Read());
        CyDelay(tiempo);
        switch(SW_Read()) {
            case 0xE:
                tiempo = 100;
                break;
            case 0xD:
                tiempo = 250;
                break;
            case 0xB:
                tiempo = 500;
                break;
            case 0x7:
                tiempo = 1000;
                break;
            default:
                break;
        }
    }   
}

/* [] END OF FILE */
